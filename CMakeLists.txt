project(sparse-de)
cmake_minimum_required(VERSION 3.0)
list(APPEND CMAKE_CXX_FLAGS "-std=c++11")

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib)

add_subdirectory(lib)
add_executable(sparse main.cpp)
target_link_libraries(sparse sparse-de-st)
