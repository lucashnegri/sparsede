#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include "sparse-de.h"

using namespace std;
using namespace sde;

// parameters
const int POP = 100;
const double SIGMA = 1e-3;
const double SP0 = 1e-2;
const double F = 0.7;
const double CR = 0.9;
const int SEED = -1;

static Mtx load_matrix(const char* path, int rows, int cols)
{
    ifstream inp;
    inp.open(path);
    if(!inp.good()) throw runtime_error("Could not load matrix");

    Mtx out(rows, cols);

    for(int i = 0; i < rows; ++i)
        for(int j = 0; j < cols; ++j)
            inp >> out(i,j);

    return out;
}

static double SNR(const Vec& sig, const Vec& out)
{
    const Eigen::Map<Vec> est(const_cast<double*>(&out[0]), out.size());
    const double signal = sig.squaredNorm();
    const double noise  = (est - sig).squaredNorm();
    return 10.0 * std::log10(signal / noise);
}

int main(int argc, char* argv[])
{
    if(argc != 6)
    {
        cerr << "Usage: sparse A y x OBS DIM" << endl;
        return 0;
    }

    const int OBS = atoi(argv[4]);
    const int DIM = atoi(argv[5]);

    Mtx A = load_matrix(argv[1], OBS, DIM);
    Vec y = load_matrix(argv[2], OBS, 1);
    Vec x = load_matrix(argv[3], DIM, 1);

    if( (A.cols() != x.rows()) || (A.rows() != y.rows()) )
    {
        cerr << "Invalid matrix sizes" << endl;
        return 0;
    }

    auto out = sde::reconstruct(A, y, 0.0, 0.3, 5000, 1000, SIGMA, SP0,
                                POP, F, CR, SEED);
    cerr << setprecision(3);

    for(int i = 0; i < DIM; ++i)
        cerr << out[i] << "\n";

    cout << SNR(x, out) << endl;

    return 0;
}

