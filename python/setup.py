from setuptools import setup

setup(
    name="sde",
    packages=["sde"],
    version="0.2.0",
    description="Sparse reconstruction by using differential evolution",
    author="Lucas Hermann Negri",
    author_email="lucashnegri@gmail.com",
    url="http://oproj.tuxfamily.org",
    keywords=["reconstruction", "sparse", "math"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
)
