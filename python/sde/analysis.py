import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as la
import itertools
import math
import copy
import random
from .wrapper import evaluate

figsize = (10, 6)
colors = itertools.cycle(['r', 'g', 'b', 'c', 'm', 'y', 'k'])

class ListTable(list):

    def _repr_html_(self):
        html = ["<table>"]
        for row in self:
            html.append("<tr>")

            for col in row:
                html.append("<td>{}</td>".format(col))

            html.append("</tr>")
        html.append("</table>")
        return ''.join(html)


def SNR(x0, x):
    """
    Computes the signal to noise ratio, in dB, between the reference (x0)
    and the computed (x) signal.
    """
    x0 = x0.ravel()
    x = x.ravel()
    err = x - x0
    return 10 * math.log10(np.sum(x0 ** 2) / np.sum(err ** 2))

def compute_db_stats(target, rec, thres):
    """
    Returns the mean, std, and convergence values from the experiment data.
    """
    snrs = [SNR(x0, x) for x0, x in zip(target, rec)]
    mean = np.mean(snrs)
    std = np.std(snrs, ddof=1)
    conv = (np.asarray(snrs) >= thres).sum() * 100.0 / len(snrs)

    return mean, std, conv

def summary(target, rec, thres):
    """
    Prints a report to stdout with the mean, std, and percentage of
    reconstructions that converged to the correct solution (SNR greater or
    equal to thres).
    """
    mean, std, conv = compute_db_stats(target, rec, thres)
    print('SNR: {:.2f} +/- {:.2f}, {:.2f}% converged'.format(mean, std, conv))


def expand(indexes, total, masses):
    """
    Convers a list with the positions where the mass is different than zero,
    with the corresponding mass to a `dense` list with all the information.

    >>> expand([1,3], 3, [100.0,200.0])
    [100.0, 0.0, 200.0]
    """

    list_ = [0.0] * total

    for i, m in zip(indexes, masses):
        list_[i - 1] = m

    return list_


def load(path, raw=False, acc=False):
    """
    Loads the readings at `path`, returning the time axis in seconds and the
    readings (peak difference by default or raw if `raw` is True). If raw is
    not True, then the first reading will be used as a reference and will be
    ommited from the output. The `acc` flag controls if the time must be
    accumulated.
    """
    mtx = np.loadtxt(path)
    time = mtx[:, 0] / 1000.0
    readings = mtx[:, 1:]

    if not raw:
        time = time[1:]
        readings = (readings - readings[0])[1:]  # substract and skip

    return time, readings


def sample(samples, total, masses):
    """
    Returns a number of `samples` from a given `total` of possible points.
    The samples are performed to avoid duplicates in `masses`.
    """
    indexes_list = []
    image = set()
    rng = random.Random()

    while len(indexes_list) < samples:
        indexes = rng.sample(range(1, total + 1), len(masses))
        img = tuple(expand(indexes, total, masses))

        if img not in image:
            image.add(img)
            indexes_list.append(indexes)

    return indexes_list


def additivity(apart, united, no_title=False):
    """
    Verifies the linearity of the system by using the additivity property.
    """
    fig, ax = plt.subplots(figsize=figsize)
    width = 0.35
    bottom = np.zeros(len(apart[0]))
    ind = np.arange(len(apart[0]))
    local = copy.deepcopy(colors)

    for reading in (r.ravel() for r in apart):
        ax.bar(ind, reading, width=width, color=next(local), bottom=bottom,
               zorder=1)
        bottom += reading

    ax.scatter(ind + width / 2.0, united, color='black', s=100, zorder=2)
    ax.set_xticks(width / 2.0 + ind)
    ax.set_xticklabels(list(map(str, ind + 1)))
    ax.set_xlabel('FBG')
    ax.set_ylabel('Peak change [nm]')

    if not no_title:
        ax.set_title('Linearity - Additivity')

    ax.grid()
    fig.tight_layout()

    return fig, ax

def homogeneity(readings, no_title=False):
    """
    Verifies the linearity of the system by using the homogeneity property.
    """
    fig, ax = plt.subplots(figsize=figsize)
    ind = np.arange(len(readings[0]))
    local = copy.deepcopy(colors)

    for i, reading in enumerate(readings):
        ax.scatter(ind, reading, color=next(local), s=100)

        if i == 0:
            continue

        for j, (x, y) in enumerate(zip(ind, reading)):
            times = y / readings[i - 1, j]
            an = "(x{:.1f})".format(times)
            ax.annotate(an, (x + 0.1, y))

    ax.set_xticks(ind)
    ax.set_xticklabels(list(map(str, ind + 1)))
    ax.set_xlabel('FBG')
    ax.set_ylabel('Peak change [nm]')

    if not no_title:
        ax.set_title('Linearity - Homogeneity')

    ax.grid()
    fig.tight_layout()

    return fig, ax

def hysteresis(time, readings, no_title=False):
    """
    Verifies the hysteresis of the system by sequentially plotting the
    acquired values.
    """
    fig, ax = plt.subplots(figsize=figsize)
    lines = ax.plot(time, readings)
    ax.legend(lines, ["FBG {}".format(x + 1) for x in range(readings.shape[1])])
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Peak change [nm]')

    if not no_title:
        ax.set_title('Hysteresis')

    ax.grid()
    fig.tight_layout()
    return fig, ax

def convergence(problem, out, THRESHOLD):
    """
    Analyses the convergence of the problem instance to detect the origin of
    the errors (convergencet to a local minimum or problem ambiguity).

    Outputs a table with the analysis results.

    Can only be used with results from the SDE method.
    """
    lt = ListTable()
    lt.append(["#", "<b>SNR</b>", "<b>Fitness (cand)</b>",
               "<b>Fitness (target)</b>", "<b>Status</b>"])

    for i in range(len(out)):
        snr = SNR(problem.X[i], out[i])
        cand_fit = evaluate(problem.A, problem.Y[i], out[i],
                            problem.sigma, problem.sp0)
        target_fit = evaluate(problem.A, problem.Y[i], problem.X[i],
                              problem.sigma, problem.sp0)

        if snr < THRESHOLD:
            if target_fit >= cand_fit:
                status = "<font color=\"red\">Local minimum</font>"
            else:
                status = "<font color=\"blue\">Ambiguity</font>"
        else:
            status = "<font color=\"green\">Converged</font>"

        lt.append([i + 1, "{:.3f}".format(snr), "{:.3g}".format(cand_fit),
                   "{:.3g}".format(target_fit), status])

    return lt

def quality(problem):
    """Computes the mean difference ration between the experimental readings (Y) and the Y estimated
    by fixing A and using the known X. A value close to 1 indicates that the system is perfectly
    linear."""
    err = np.absolute(problem.Y - problem.estimate_y())
    eps = 1e-12
    perr = err / (np.abs(problem.Y) + eps)
    perr[perr > 1.0] = 1.0
    return 1.0 - np.mean(perr)

def sensitivity(v, no_title=False):
    """
    Verifies the mean sensitivity of each FBG. Should call np.absolute first?
    """
    fig, ax = plt.subplots(figsize=figsize)
    ind = np.arange(v.shape[1])
    plt.errorbar(ind, np.mean(v, axis=0), yerr=np.std(v, axis=0),
                 linestyle='None', marker='o')
    ax.set_xticks(ind)
    ax.set_xticklabels(list(map(str, ind + 1)))
    ax.set_xlabel('FBG')
    ax.set_ylabel('Mean peak change [nm]')

    if not no_title:
        ax.set_title('Sensitivity per FBG')

    ax.set_xlim(-1, v.shape[1])
    ax.grid()
    fig.tight_layout()

    return fig, ax

def extract_calibration_matrix(X, Y):
    n, m = Y.shape[1], X.shape[1]
    n_placements = X.shape[0]

    sys_A = []
    sys_Y = []

    for eq in range(n_placements):
        x = X[eq]
        y = Y[eq]

        for i in range(n):
            left = np.zeros(n * m)
            of = i * m
            left[of:of + m] = x
            right = y[i]
            sys_A.append(left)
            sys_Y.append(right)

    sys_A = np.array(sys_A)
    sys_Y = np.array(sys_Y)
    assert(sys_A.shape == (n * n_placements, m * n))

    return la.lstsq(sys_A, sys_Y)[0].reshape(n, m)