"""This module provides a binding os sparse-de to Python, allowing signal
reconstruction in under-determined systems (compressed sensing)"""

import ctypes as ct
import numpy as np
import numpy.ctypeslib as npct
import sys

if sys.platform == 'win32':
    _LIB = ct.cdll.LoadLibrary("sparse-de.dll")
else:
    _LIB = ct.cdll.LoadLibrary("libsparse-de.so")

_FN = _LIB.sde_reconstruct

_FN.argtypes = [
    npct.ndpointer(dtype=np.double, ndim=2, flags='C_CONTIGUOUS'),
    npct.ndpointer(dtype=np.double, flags='C_CONTIGUOUS'),
    npct.ndpointer(dtype=np.double, flags='C_CONTIGUOUS'),
    ct.c_int, ct.c_int, ct.c_double, ct.c_double,
    ct.c_int, ct.c_int, ct.c_double, ct.c_double, ct.c_int,
    ct.c_double, ct.c_double, ct.c_int
]

_FN.restype = None


def reconstruct(sensing_matrix, observations, lower, upper,
                n_max, n_stuck, sigma, sp0, n_pop, f, cr, seed):
    """ Returns the reconstructed signal from the sensing matrix A and the
    observation vector y.

    >>> import numpy as np
    >>> import sde
    >>> A = np.array([[1.,2.],[4.,6.]])
    >>> y = np.array([10.,34.])
    >>> sde.reconstruct(A, y, 0., 5., 1000, 100, 0., 0., 40, 0.8, 0.35, -1)
    array([ 4.,  3.])

    """
    obs, dim = sensing_matrix.shape
    reconstruction = np.empty(dim)
    _FN(sensing_matrix, observations, reconstruction, obs, dim,
        lower, upper, n_max, n_stuck, sigma, sp0, n_pop, f, cr, seed)

    return reconstruction


_FN2 = _LIB.sde_evaluate

_FN2.argtypes = [
    npct.ndpointer(dtype=np.double, ndim=2, flags='C_CONTIGUOUS'),
    npct.ndpointer(dtype=np.double, flags='C_CONTIGUOUS'),
    npct.ndpointer(dtype=np.double, flags='C_CONTIGUOUS'),
    ct.c_int, ct.c_int, ct.c_double, ct.c_double
]

_FN2.restype = ct.c_double


def evaluate(sensing_matrix, observation, candidate, sigma, sp0):
    """Evaluates a candidate solution, returning the error that the solver
    would use in the fitness function. See `reconstruct` for the parameter
    details.
    """

    obs, dim = sensing_matrix.shape
    return _FN2(sensing_matrix, observation, candidate, obs, dim, sigma, sp0)
