'''Helper functions and classes to deal with common cases of the sparse
reconstruction problem.'''

import numpy as np
from sde import reconstruct
from sde import parser
from sde.analysis import SNR

class SparseRecProblem:
    """Helper class to deal with a sparse reconstruction problem."""
    def __init__(self, path=None, wanted_sens=None):
        # Problem structure
        self.A = None
        self.Y = None
        self.X = None

        # default parameters
        self.lower = 0.0
        self.upper = 1.0
        self.n_max = 2000
        self.n_stuck = 200
        self.sigma = 1e-4
        self.sp0 = 1e-3
        self.n_pop = 100
        self.f = 0.8
        self.cr = 0.35
        self.seed = -1

        if path:
            self.load(path)

        if wanted_sens is not None:
            self.A = self.A[wanted_sens, :].copy()
            self.Y = self.Y[:, wanted_sens].copy()

    def load(self, path):
        """Loads the problem structure into memory. Path should be the
        directory containing the info file.
        """
        self.A, self.X, self.Y = parser.load(path)

        self.READINGS = self.X.shape[0]
        self.SENSORS = self.A.shape[0]
        self.POSITIONS = self.A.shape[1]

    def solve(self, A, y):
        """Solves one instance of the reconstruction problem, returning the
        reconstructed vector.
        """
        return reconstruct(A, y, lower=self.lower, upper=self.upper,
                           n_max=self.n_max, n_stuck=self.n_stuck,
                           sigma=self.sigma, sp0=self.sp0, n_pop=self.n_pop,
                           f=self.f, cr=self.cr, seed=self.seed)

    def evaluate_all(self, solve=None):
        """Tries to solve every problem instance by using the solve function
        (self.solve by default), returning an array with the reconstructed
        signals.
        """
        if not solve:
            solve = self.solve

        return np.array([solve(self.A, inst) for inst in self.Y])
    
    def estimate_y(self):
        """Estimates and returns the correct Y matrix assuming that A is constant and X is
        correct."""
        ny = self.Y.copy()
        
        for exp in range(self.X.shape[0]):
            ny[exp, :] = self.A @ self.X[exp, :]
            
        return ny
    
    def sensitivity(self):
        """Estimates the sensitivity of each transducer, based on the calibration matrix A."""
        return np.absolute(self.A).mean(axis=1)
    
    def normalize(self):
        """Normalize the sensitivity to approximately 1 [insert unit here]. This modifies both
        A and Y matrices."""
        sens = self.sensitivity()
        
        for r in range(self.A.shape[0]):
            self.A[r] /= sens[r]
            self.Y[:, r] /= sens[r]