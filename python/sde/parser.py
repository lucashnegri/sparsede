"""Utilities for parsing a Python file describing the positions of the masses
in each placement performed in the analysed experiment.
"""
import numpy as np
import os
from sde.analysis import expand

def perm(length, value):
    """Generates all lists with the distinct permutations of zeros and the
    value v.

    >>> for p in perm(3, 0.2):
    ...     print(p)
    [0.2, 0.0, 0.0]
    [0.0, 0.2, 0.0]
    [0.0, 0.0, 0.2]
    """
    for i in range(length):
        list_ = [0.]*length
        list_[i] = value
        yield list_


def fix(iterable, pos, value):
    """Fixes `value` at position `pos` in each element of `iterable`,
    generating a new iterable.

    >>> for p in fix(perm(3, 0.2), 1, 0.5):
    ...     print(p)
    [0.2, 0.5, 0.0, 0.0]
    [0.0, 0.5, 0.2, 0.0]
    [0.0, 0.5, 0.0, 0.2]
    """
    for list_ in iterable:
        list_.insert(pos, value)
        yield list_


def fromindexes(indexes, total, masses):
    """Given a list of `indexes` (one-based) where the `masses` are placed,
    returns the lists with mass values in every position (sparse representation
    to the dense one expected by `sde`.

    >>> for p in fromindexes([(1, 4), (2, 3)], 5, (200.0, 100.0)):
    ...      print(p)
    [200.0, 0.0, 0.0, 100.0, 0.0]
    [0.0, 200.0, 100.0, 0.0, 0.0]
    """
    for idx_list in indexes:
        yield expand(idx_list, total, masses)


def parse(path):
    """Parses the configuration file at `path`, returning the positions of
    each reading and the index of the reference reading.
    """
    g = {'perm': perm, 'fix': fix, 'fromindexes': fromindexes}
    exec(open(path).read(), g)
    return g['positions']


def load(path, have_calibration=True):
    """Loads the data files from directory at `path`, returning the calibration
    file, the original positions, and the sensor values.
    """
    # load data files
    A = np.loadtxt(os.path.join(path, 'calibration.txt')) if have_calibration else None

    peaks = np.loadtxt(os.path.join(path, 'peaks.txt'))
    positions = parse(os.path.join(path, 'positions.py'))

    # get the reference
    ref = peaks[0]  # reference is always the first
    aux = np.delete(peaks, 0, axis=0)

    # get the peak shift, ignoring the time column
    Y = (aux - ref)[:, 1:]
    X = np.asarray(positions)

    return A, X, Y
