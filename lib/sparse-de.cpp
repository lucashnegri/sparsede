#include <algorithm>
#include <solver-de.hpp>
#include <iostream>
#include "sparse-de.h"

using namespace std;
using namespace SolverDE;
using namespace Eigen;

const double eps = 1e-2;

namespace sde
{
    struct SparseFitness
    {
        double operator()(ConstRef cand) const
        {
            double sparse = 0;
            for(double val: cand)
                sparse += std::pow(std::max(eps - std::abs(val), 0.) / eps, 2.0);

            const Map<Vec> x(const_cast<double*>(&cand[0]), cand.size());
            return - ((A*x - y).squaredNorm() + sigma*x.lpNorm<1>() ) + sparse*sp0; 
        }

        Mtx A;
        Vec y;
        double sigma, sp0;
    };
    
    Vec reconstruct(const Mtx& A, const Vec& y, double lower, double upper,
                    int max_iters, int stuck_iters, double sigma, double sp0,
                    int pop, double f, double cr, int seed)
    {
        Vec x(A.cols());
        
        sde_reconstruct(A.data(), y.data(), x.data(), A.rows(), A.cols(),
                lower, upper, max_iters, stuck_iters, sigma, sp0,
                pop, f, cr, seed); 
        return x;
    }
}

extern "C" void sde_reconstruct(const double* A, const double* y, double* x,
    int obs, int pos, double lower, double upper, int max_iters, int stuck_iters,
    double sigma, double sp0, int pop, double f, double cr, int seed)
{
    Solver<sde::SparseFitness> solver(pop, pos, seed);
    solver.f = f;
    solver.cr = cr; 

    for(int i = 0; i < pos; ++i)
    {
        solver.limits[i][0] = lower;
        solver.limits[i][1] = upper;
    }
    
    auto& fit = solver.fitness;
    fit.A = Map<sde::Mtx>(const_cast<double*>(A), obs, pos);
    fit.y = Map<sde::Vec>(const_cast<double*>(y), obs);
    
    fit.sigma = sigma;
    fit.sp0 = sp0;
    
    solver.run(max_iters, stuck_iters);
    
    auto ss = solver.best();
    
    for(int i = 0; i < pos; ++i)
        x[i] = get<1>(ss)[i];
}

extern "C" double sde_evaluate(const double* A, const double* y, double* x,
                             int obs, int pos, double sigma, double sp0)
{
    sde::SparseFitness fit;
    fit.A = Map<sde::Mtx>(const_cast<double*>(A), obs, pos);
    fit.y = Map<sde::Vec>(const_cast<double*>(y), obs);
    fit.sigma = sigma;
    fit.sp0 = sp0;

    Array2D cand(boost::extents[1][pos]);
    for(int i = 0; i < pos; ++i)
        cand[0][i] = x[i];

    return fit(cand[0]); 
}
