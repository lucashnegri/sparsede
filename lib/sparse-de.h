#ifndef SPARSE_DE_H
#define SPARSE_DE_H

#ifdef __cplusplus

extern "C"
{
    void sde_reconstruct(const double* A, const double* y, double* x, int obs, int pos,
                         double lower, double upper, int max_iters, int stuck_iters,
                         double sigma, double sp0, int pop, double f, double cr, int seed);

    double sde_evaluate(const double* A, const double* y, double* x, int obs, int pos,
                        double sigma, double sp0); 
}
#endif

#ifdef __cplusplus

#include <Eigen/Dense>
namespace sde
{
    typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Mtx;
    typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::RowMajor> Vec;
    
    Vec reconstruct(const Mtx& A, const Vec& y, double lower, double upper,
                    int max_iters, int stuck_iters, double sigma, double sp0,
                    int pop, double f, double cr, int seed);
}
#endif

#endif
