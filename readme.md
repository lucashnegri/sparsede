# SparseDE

## About

SparseDE implements a compressive sensing reconstruction algorithm by using
differential evolution. Tries to reconstruct the original data by searching for
a solution that minimizes a cost function composed by the L2 error to the
observations, the L1 norm of the solution, with an additional aproximation to
the L0 norm.

## Dependencies

Depends on SolverDE and Eigen 3. The Python bindings depends on Python 3.

## Contact

Lucas Hermann Negri <lucashnegri@gmail.com>
